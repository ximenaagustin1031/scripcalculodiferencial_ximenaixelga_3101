%Octave Script
%Title             : Clasificaciòn de los nùmeros
%Description       : Script para recordar conceptos de nùmeros
%Author            : Ximena Ixel Garcìa Agustìn
%Date              : 20210928
%Version           : 1
%Usage             : octave/ClasificacionNùmeros
%Notes             : Requiere aplicaciòn octave, usar su linea de comandos
%                  : https://octaveintro.readthedocs.io/n/latest/index.html

clear
c_numeros_Naturales = '?= {1, 2, 3, ....n} si n > 0';
c_numeros_Enteros = '?={-n..., -2,-1, 0, 1, 2,...n}';
c_numeros_Racionales='?={m/n dónde m,n ?? n? 0}';
c_numeros_Irracionales='I = {?n que no puede ser expresada como ? todas las raices de n no son exactas}';
c_numeros_Reales='?={I, ?, ?, ?}';

%propiedades de los números, sean a,b,c,d,e ??

%Propiedades de ?(cerradura)
p_cerradura='a + b ??';
p_cerradura2='ab ??';
p_cerradura3= '7+9 ??';
p_cerradura4= '?= pertenecia';
a=3;
b=5;
a+b
a*b

%Propiedad asociativa
p_asociativa= 'a+( b+c)';
p_asociativa2= 'a(b c)=(a b) c';
p_asociativa='3 +(8-10)=(8+3)-10';
a=1;
b=3;
c=4;
(b*c)
(a*b)

%Propiedad conmutativa
p_conmutativa='a+b=b+a';
p_conmutativa2= 'a b= b a';
a=2;
b=5;
b+a
b*a

%Propiedad distributiva
p_distributiva=' a(b+c)=a b+ a c';
a=2;
b=7;
c=3;
a*(b+c)
a*b + a*c

%Neutro aditivo
p_neutroA=' a +0=a';
p_neutroA2= 'Ojo: a + 0 = 0 + a ---> es conmutativa';
a=5
a+0

%Neutro multiplicativo
p_neutroM=' a(1)=a';
a=5;
a*1

%Inverso aditivo
p_InversoA='a + -a=0';
a=3;
a+(-a)

%Inverso multiplicativo o reciproco
p_inversoM='a (1/a)=1';
a=1;
a*(1/a)

%Propiedad transitiva(| entonces)
p_transitiva='sí a>b y b>c | a>c';
p_transitiva2='sí a=b y b=c | a=c';
a=5;
b=3;
a>c
a=c

%Tricotomía( raíz del algebra) siempre se pueden comparar
p_tricotomia='a>b';
p_tricotomia2= 'a=b';
p_tricotomia3= 'a<b';
a=1;
b=1;
a=b
1
%Signos de agrupación
s_agrupacion= '  {[()]}';



